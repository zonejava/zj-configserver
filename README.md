# Zone Java project - External config server
## Description
This edge-microservice is used to access external configurations files for 
Zone Java microservices.
Follow [this link](https://gitlab.com/zonejava/conception/-/blob/master/README.md) to see the project overview.
## Development
It has been developed with IntelliJ IDEA 2020.1 (Ultimate Edition) and Java JDK 11.0.7.

It uses SpringBoot 2.2.5.RELEASE and spring cloud config server.

You will find dependencies uses details in pom.xml files.
## Owner
Arnaud Laval - arnaudlaval33@gmail.com

The Zone Java project is developed as part of final project of DA Java cursus (OpenClassrooms)
