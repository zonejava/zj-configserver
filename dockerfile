FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/config-server.jar config-server.jar
ENTRYPOINT ["java","-jar","config-server.jar"]